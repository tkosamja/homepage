function startTime() {
    var today = new Date();
    var weekday = today.getDay(); 
    var hour = today.getHours();
    var minutes = today.getMinutes();
    var month = today.getMonth();
    var day = today.getDate();
    var year = today.getFullYear();
    var seconds = today.getSeconds(); 
    var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    
    hour = checkTime(hour);
    minutes = checkTime(minutes);
    seconds = checkTime(seconds);
    
    if (day == "1") {
      day = "1st";
    }
    
    else if (day == "2") {
      day = "2nd";
    }
    
    else if (day == "3") {
      day = "3rd";
    }
    
    else {
      day = day + "th";
    }
    
    document.getElementById("date").innerHTML = "Today is " + days[weekday] + ", " + months[month] + " " + day + ".";
    document.getElementById("clock").innerHTML = hour + ":" + minutes + ":" + seconds;
    var today2 = new Date().getHours();
    
    if (hour >= 6 && hour <= 11) {
        document.getElementById("greeting").innerHTML = "Good morning, user.";		
}

    else if (hour >= 12 && hour <= 16) {
        document.getElementById("greeting").innerHTML = "Good afternoon, user.";
}
	
    else if (hour >= 17 && hour <= 19) {
        document.getElementById("greeting").innerHTML = "Good evening, user.";
}

    else if (hour >= 20 && hour <= 23) {
        document.getElementById("greeting").innerHTML = "Good night, user.";
}

    else {
        document.getElementById("greeting").innerHTML = "Good night, user.";
}

    setTimeout(function(){startTime()}, 500)
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}