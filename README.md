# homepage
Theme adapted from https://github.com/hidanoki/homepage. Original credits belong to him.
I just made some small changes, like being able to directly type a website url in the input, a greeting, the date and time. I also included a mobile version. You can also hover some link so it shows website subdomains. Sometimes I use the box with outline, sometimes I don't; I change it according to my likings. I changed some search bar prefixes.

Theres a search bar at the bottom that uses prefixes such as !g (google) or !y (youtube) to search different websites (code from twily: http://twily.info/firefox/). These can be edited in the .js file.

Available search websites and their prefixes:

    Default: StartPage
    !g: Google
    !i: Google Images
    !y: Youtube
    !w: Wikipedia
    !t: Google Translate
    
If it doesn't fit your screen/work properly, please try to change the css or notify me. I made this for my screen resolution so it's possible it doesn't work in other.
Also, for some reason I am not aware off the site gets disfigured with Chrome. 

![](https://s1.postimg.org/2a22x7nw27/Screenshot_from_2017-10-06_12-26-25.png)